class Counter:
    def __init__(self):
        self.value = 0
    def incr(self):
        self.value += 1
    def decr(self):
        self.value -= 1
    def incrby(self, number):
        self.value += number
    def decrby(self, number):
        self.value -= number

value = Counter()
print(value.value)
value.incr()
print(value.value)
value.incr()
print(value.value)
value.decr()
print(value.value)
value.incrby(10)
print(value.value)
value.decrby(5)
print(value.value)
