from math import sqrt
class Triangle:

    def __init__(self):
        self.points = []
        self.ab = 0
        self.ac = 0
        self.bc = 0

    def add_point(self, point):
        if len(self.points)==3:
            print("more than three points can't be added to a triangle")
            return
        self.points.append(point)

    @staticmethod
    def is_traingle(self):
        if self.ab+self.ac > self.bc or self.ab+self.bc > self.ac or self.ac+self.bc > self.ab:
            return True
        return False

    def perimeter(self):
        """
        Take traingle object and calculate perimeter

        :param self : traingle instance which contain three  verices of traingle as attributes.
        :type self : instance of class traingle.
        :return : perimeter of triangle.
        :rtype : float.
        """
        if len(self.points) == 3:
            #ab ,ac and bc are edges of triangle calculated with distance formula.
            self.ab = sqrt(((self.points[0][0] - self.points[1][0])**2)\
                    + ((self.points[0][1] - self.points[1][1])**2))
            self.ac = sqrt(((self.points[0][0] - self.points[2][0])**2)\
                    + ((self.points[0][1] - self.points[2][1])**2))
            self.bc = sqrt(((self.points[1][0] - self.points[2][0])**2)\
                    + ((self.points[1][1] - self.points[2][1])**2))
            if self.is_traingle(self):
                return self.ab+self.ac+self.bc
            return "NOT A TRIANGLE"
        else:
            print("A triangle should contain atleast 3 points.")
    def is_equal(self, triangle):
        if set(self.points) == set(triangle.points):
            return True
        return False
    def __eq__(self,triangle):
        return set(self.points) == set(triangle.points)

t1 = Triangle()
t1.add_point((0, 0))
t1.add_point((0, 3))
t1.add_point((4, 0))
t1.add_point((5,0))
print(t1.points)
print('perimeter', t1.perimeter())
print()

t2 = Triangle()
t2.add_point((1, 2))
t2.add_point((2, 1))
t2.add_point((1, 5))
print(t2.points)
print('perimeter', t2.perimeter())
print()

t3 = Triangle()
t3.add_point((1, 2))
t3.add_point((2, 1))
t3.add_point((1, 5))

print(t1 == t3)
print(t1.is_equal(t3))
print(t3.is_equal(t1))
print()

print(t2 == t3)
print(t2.is_equal(t3))
print(t3.is_equal(t2))
print()

t4 = Triangle()
t4.add_point((1, 1))
t4.add_point((1, 1))
t4.add_point((1, 1))
print(t4.perimeter())

t4 = Triangle()
t4.add_point((1, 1))
t4.add_point((1, 1))
print(t4.perimeter())
